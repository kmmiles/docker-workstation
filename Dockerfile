FROM registry.gitlab.com/kmmiles/docker-nomachine:latest

RUN set -eux; apt-get update && \
    apt-get -y install --no-install-recommends \
      curl \
      wget \
      gnupg && \
    curl -sL https://deb.nodesource.com/setup_8.x | bash - && \
    curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list && \
    curl -fSL https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb -o chrome.deb && \
    dpkg -i chrome.deb && \
    rm -f chrome.deb && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
      bash \
      sudo \
      gcc \
      g++ \
      make \
      htop \
      xterm \
      pv \
      wget \
      vim \
      screen \
      cmake \
      golang \
      nodejs \
      yarn \
      git \
      pwgen && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN rm -f /bin/sh ; ln -sf /bin/bash /bin/sh

USER nomachine

RUN cd /home/nomachine && \
    git clone https://gitlab.com/kmmiles/dotfiles.git && \
      cd dotfiles && \
      chmod +x bin/install && \
      bin/install && \
      cd .. && \
      rm -rf dotfiles

USER root
